package com.ia.sharedpreferencesmanager;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MostrarDatosActivity extends AppCompatActivity {

    TextView tvNombre;
    TextView tvLastName;
    TextView tvEmail;
    TextView tvEdad;
    Button btnClear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_datos);

        tvNombre = (TextView)findViewById(R.id.tv_nombre);
        tvLastName = (TextView)findViewById(R.id.tv_lastName);
        tvEmail = (TextView)findViewById(R.id.tv_email);
        tvEdad = (TextView)findViewById(R.id.tv_edad);
        btnClear = (Button)findViewById(R.id.btn_clear);

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferencesManager.clearUserData(MostrarDatosActivity.this);
                Intent intent = new Intent(MostrarDatosActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        User user = SharedPreferencesManager.getUserData(this);
        tvNombre.setText(user.getName());
        tvLastName.setText(user.getLasName());
        tvEmail.setText(user.getEmail());
        tvEdad.setText(String.valueOf(user.getAge()));
    }
}
